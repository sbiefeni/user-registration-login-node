
function UserModel(sequelize, types, table) {
  return sequelize.define(table, {
    userId: {
      primaryKey: true,
      type: types.UUID,
      defaultValue: types.UUIDV4,
    },
    email: {
      type: types.STRING,
      required: true,
      minlength: 5,
      maxlength: 255,
      unique: true,
    },
    termsAccepted: {
      type: types.DATE,
      defaultValue: types.NOW,
    },
    hashedPassword: {
      type: types.STRING,
      required: true,
      maxlength: 1024,
    },
    passwordSalt: {
      type: types.STRING,
      required: true,
      minlength: 5,
      maxlength: 1024,
    },
    createdAt: {
      type: types.DATE,
    },
    updatedAt: {
      type: types.DATE,
    },
  });
}

export default UserModel;
