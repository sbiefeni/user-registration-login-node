import Sequelize from 'sequelize';
import bcrypt from 'bcryptjs';
import { errors } from '@test/library-logging';

import UserModel from '../models/user.js';

class UserService {
  /**
   * Initializes Sequelize with the given configuration along with the user models.
   *
   * @param {object} configuration Configuration to be used for Sequelize.
   * Note that storage is only used in test when we use SQLLite
   * SequelizeMock for testing.
   */
  async init({
    port, database, host, dialect, password, username, storage,
  }) {
    this.sequelize = new Sequelize({
      username, password, database, host, port, dialect, storage, logging: process.env.NODE_ENV !== 'test',
    });
    await this.sequelize.authenticate();

    this.UnverifiedUser = UserModel(this.sequelize, Sequelize, 'unverifiedUsers');
    this.VerifiedUser = UserModel(this.sequelize, Sequelize, 'verifiedUsers');

    // TODO: stop using sync and use migrations
    // TODO: in production
    await this.sequelize.sync();
  }

  /**
   * Creates the user in the unverified_users table if it doesn't exist in either of tables.
   * The e-mail address that is specified on the user object is used for the lookup.
   *
   * @param {object} user Contains the data values associated with the UserModel object.
   */
  async createUser(email, password, termsAccepted) {
    return this.sequelize.transaction(async (transaction) => {
      const unverifiedUser = await this.UnverifiedUser.findOne({
        where: { email },
        transaction,
      });
      const verifiedUser = await this.VerifiedUser.findOne({
        where: { email },
        transaction,
      });

      if (unverifiedUser !== null || verifiedUser !== null) {
        throw new errors.Conflict('User already exists.');
      }
      const passwordSalt = await bcrypt.genSalt(10);
      const hashedPassword = await bcrypt.hash(password, passwordSalt);

      const newUser = await this.UnverifiedUser.create({
        email,
        hashedPassword,
        termsAccepted,
        passwordSalt,
      }, { transaction });

      return newUser.dataValues;
    });
  }

  /**
   * Moves the user from the unverified_users table to the verified_users table.
   *
   * @param {string} id UUIDv4 that uniquely identifies the user.
   * @returns {object} The newly created user.
   */
  async verifyUser(id) {
    return this.sequelize.transaction(async (transaction) => {
      const unverifiedUser = await this.UnverifiedUser.findOne({
        where: { userId: id },
        transaction,
      });
      const verifiedUser = await this.VerifiedUser.findOne({ where: { userId: id }, transaction });

      if (verifiedUser !== null) {
        return verifiedUser;
      }

      if (unverifiedUser === null) {
        throw new errors.NotFound('Did not find the user associated with the e-mail address.');
      }

      const newlyVerifiedUser = await this.VerifiedUser.create(
        unverifiedUser.dataValues,
        { transaction },
      );
      await unverifiedUser.destroy();

      return newlyVerifiedUser.dataValues;
    });
  }

  /**
   * Finds a user in either the verified or unverified states by ID.
   *
   * @param {string} id UUIDv4 that uniquely identifies the user.
   * @returns {object} If found, returns the user; otherwise, returns null.
   */
  async findUserById(id) {
    return this.sequelize.transaction(async (transaction) => {
      // keep verified users first to ensure faster response times
      const verifiedUser = await this.VerifiedUser.findOne({
        where: { userId: id },
        transaction,
      });
      if (verifiedUser !== null) return verifiedUser.dataValues;

      const unverifiedUser = await this.UnverifiedUser.findOne({
        where: { userId: id },
        transaction,
      });
      if (unverifiedUser !== null) return unverifiedUser.dataValues;

      return null;
    });
  }

  /**
   * Finds a user in either the verified or unverified states by e-mail.
   *
   * @param {string} email Unique e-mail address that is associated with the the user.
   * @returns {object} If found, returns the user; otherwise, returns null.
   */
  async findUserByEmail(email) {
    return this.sequelize.transaction(async (transaction) => {
      // keep verified users first to ensure faster response times
      const verifiedUser = await this.VerifiedUser.findOne({ where: { email }, transaction });
      if (verifiedUser !== null) return verifiedUser.dataValues;

      const unverifiedUser = await this.UnverifiedUser.findOne({ where: { email }, transaction });
      if (unverifiedUser !== null) return unverifiedUser.dataValues;

      return null;
    });
  }

  /**
   * Finds a user that was registered with the e-mail address and then compares the provided
   * password with the one in the database.
   *
   * @param {string} email Unique e-mail address that is associated with the the user.
   * @param {password} password The password associated with the user.
   * @returns {object} If found and the password is valid, returns the user; otherwise,
   * returns null.
   */
  async authenticateUser(email, password) {
    const user = await this.findUserByEmail(email);
    if (!user) {
      return null;
    }

    const success = await bcrypt.compare(password, user.hashedPassword);
    if (!success) throw new errors.UnauthorizedRequest('Login failed.');

    return user;
  }
}

const userService = new UserService();

export default userService;
