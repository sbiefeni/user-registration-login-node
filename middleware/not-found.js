import { errors } from '@test/library-logging';

function notFound(req, res, next) {
  return next(new errors.NotFound(`Could not find ${req.path}.`));
}

export default notFound;
