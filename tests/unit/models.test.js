/* eslint-disable prefer-destructuring */
import {
  sequelize,
  dataTypes,
  checkModelName,
  checkPropertyExists,
} from 'sequelize-test-helpers';
import UserModel from '../../models/user.js';


describe('Verified User Model', () => {
  const User = UserModel(sequelize, dataTypes, 'verifiedUser');
  const user = new User();

  /**
   * methods in sequelize-test-helpers contain their own assertions
   */
  checkModelName(User)('verifiedUser');
  [
    'userId',
    'email',
    'termsAccepted',
    'hashedPassword',
    'passwordSalt',
    'createdAt',
    'updatedAt',
  ].forEach(
    checkPropertyExists(user),
  );
});

describe('Unverified User Model', () => {
  const User = UserModel(sequelize, dataTypes, 'unverifiedUser');
  const user = new User();

  /**
   * methods in sequelize-test-helpers contain their own assertions
   */
  checkModelName(User)('unverifiedUser');
  [
    'userId',
    'email',
    'termsAccepted',
    'hashedPassword',
    'passwordSalt',
    'createdAt',
    'updatedAt',
  ].forEach(
    checkPropertyExists(user),
  );
});
