import request from 'supertest';

import app from '../../index.js';

describe('/v1/health', () => {
  let server;
  let agent;

  beforeAll(async (done) => {
    server = app.listen(4000, (err) => {
      if (err) return done(err);
      agent = request.agent(server);
      return done();
    });
  });
  afterAll(done => server && server.close(done));

  it('Health Check -- should return 200', async () => {
    const res = await agent.get('/v1/health');
    expect(res.status).toBe(200);
    expect(res.body).toHaveProperty('status', 'OK');
  });
});
