import request from 'supertest';
import httpStatusCodes from 'http-status-codes';

import app from '../../index.js';

describe('General invalid requests', () => {
  let server;
  let agent;

  beforeAll(async (done) => {
    server = app.listen(4000, (err) => {
      if (err) return done(err);
      agent = request.agent(server);
      return done();
    });
  });
  afterAll(done => server && server.close(done));

  test('Invalid path', async () => {
    const res = await agent.get('/v1/does-not-exist');
    expect(res.status).toBe(httpStatusCodes.NOT_FOUND);
  });
});
