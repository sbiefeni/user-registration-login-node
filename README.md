[comment]: # (save a local image for the service, making it 200px wide)
[comment]: # (markdown is dumb!)
![](/docs/cerberus.png)

# Cerberus

Cerberus - a multi-headed dog that guards the gates of the Underworld to prevent the dead from leaving - is a service that handles:

* user login requests,
* user registration requests, and
* e-mail verification requests.

Upon successful registration or login, Cerberus generates a JWT with a public/private key pair and returns it along with the user ID in the response. This token is used by [Kharon] to authenticate requests. The necessary verification parameters (including the public key) is used exposed to [Kharon] via a RESTFul API endpoint.

During registration, Cerberus will dispatch an e-mail verification request to [Hermes] containing the e-mail address of the user and a verification token.

# Minimum Required Environment Variables

* CERBERUS_DB_USERNAME - Username of the PSQL server account.
* CERBERUS_DB_PASSWORD - Password of the PSQL server account.
* CERBERUS_JWT_PUBLIC_KEY - Private key to be used for token generation.
* CERBERUS_JWT_PRIVATE_KEY - Public key (associated with the private key) to be used for token verification.